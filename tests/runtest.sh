#!/bin/bash

# get tpm2-tools tests
TPM2_TOOLS_VERSION=5.7
git clone https://github.com/01org/tpm2-tools.git
pushd tpm2-tools
git checkout -b test $TPM2_TOOLS_VERSION
pushd test/integration
pushd tests
# some tests aren't executable currently. Needs to be fixed upstream.
chmod +x *.sh
popd
# copy the yaml comparison files (196e3d43)
cp fixtures/event-raw/event-*.yaml fixtures
popd
popd

TPM2_ABRMD=tpm2-abrmd
TPM2_SIM=swtpm
TPM2_TOOLS_TEST_FIXTURES=`pwd`/tpm2-tools/test/integration/fixtures
PATH=`pwd`/ibmtpm/src/:.:$PATH
abs_srcdir=`pwd`/tpm2-tools
srcdir=$abs_srcdir
export TPM2_ABRMD TPM2_SIM TPM2_TOOLS_TEST_FIXTURES PATH abs_srcdir srcdir

pushd tpm2-tools/test/integration
for t in `ls tests/*.sh`
do
    f=`basename $t`
    test=${f%%.*}
    /usr/share/automake-1.16/test-driver --test-name $test --log-file $test.log --trs-file $test.trs $t
done
all=`grep ":test-result:" *.trs | wc -l`;
pass=`grep ":test-result: PASS"  *.trs | wc -l`;
fail=`grep ":test-result: FAIL"  *.trs | wc -l`;
skip=`grep ":test-result: SKIP"  *.trs | wc -l`;
xfail=`grep ":test-result: XFAIL" *.trs | wc -l`;
xpass=`grep ":test-result: XPASS" *.trs | wc -l`;
error=`grep ":test-result: ERROR" *.trs | wc -l`;
if [[ $(($fail + $xpass + $error)) -eq 0 ]]; then
    success=0
else
    success=1
fi;
popd

echo PASSED: $pass
echo FAILED: $fail
echo SKIPPED: $skip
echo XFAIL: $xfail
echo XPASS: $xpass
echo ERROR: $error

exit $success
